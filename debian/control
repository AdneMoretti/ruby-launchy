Source: ruby-launchy
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Daniel Leidert <dleidert@debian.org>
Build-Depends: curl,
               debhelper-compat (= 13),
               gem2deb (>= 1),
               ruby-addressable,
               ruby-minitest,
               ruby-rspec,
               ruby-simplecov,
               xdg-utils
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-launchy.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-launchy
Homepage: https://github.com/copiousfreetime/launchy
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-launchy
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Provides: ruby-launchy-shim (= ${source:Version})
Breaks: ruby-launchy-shim,
        launchy
Replaces: ruby-launchy-shim,
          launchy
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Description: helper class for launching cross-platform applications
 Launchy is a helper class for launching cross-platform applications in a fire
 and forget manner. There are application concepts (browser, email client, etc)
 that are common across all platforms, and they may be launched differently on
 each platform. Launchy is here to make a common approach to launching external
 applications from within ruby programs.
