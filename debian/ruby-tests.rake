require 'gem2deb/rake/testtask'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs << 'spec' << 'libs'
  t.test_files = FileList['spec/**/*_spec.rb']
  t.warning = true
  t.verbose = true
end
